const UserDAO = require('../../../src/dao/user');
const UserFacade = require('../../../src/facades/user');

const ZERO_PERCENT_DISCOUNT = 0;
const FIVE_PERCENT_DISCOUNT = 0.05;
const TEN_PERCENT_DISCOUNT = 0.1;
const FIFTEEN_PERCENT_DISCOUNT = 0.15;

const TEST_USER_ID_1 = 1;
const TEST_USER_ID_2 = 2;
const TEST_USER_ID_3 = 3;
const TEST_USER_ID_4 = 4;
const TEST_USER_ID_5 = 5;
const TEST_USER_ID_6 = 6;
const TEST_USER_ID_7 = 7;

describe('UserFacade', () => {
  let userFacade;

  beforeEach(() => {
    userFacade = new UserFacade({ userDao: new UserDAO() });
    jest.clearAllMocks();
  })

  test("Should calculate zero percent discount", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_1)
    expect(discount).toBe(ZERO_PERCENT_DISCOUNT);
  });

  test("Should calculate five percent discount", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_2)
    expect(discount).toBe(FIVE_PERCENT_DISCOUNT);
  });

  test("Should calculate ten percent discount", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_3)
    expect(discount).toBe(TEN_PERCENT_DISCOUNT);
  });

  test("Should calculate fifteen percent discount", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_4)
    expect(discount).toBe(FIFTEEN_PERCENT_DISCOUNT);
  });

  test("Should calculate five percent tax when net price is less than threshold but with tax together is more than threshold", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_5)
    expect(discount).toBe(FIVE_PERCENT_DISCOUNT);
  });

  test("Should calculate ten percent discount if user had multiple orders", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_6)
    expect(discount).toBe(FIFTEEN_PERCENT_DISCOUNT);
  });

  test("Should calculate zero percent discount if user doesn't have any orders", () => {
    const discount = userFacade.getDiscountForUserById(TEST_USER_ID_7)
    expect(discount).toBe(ZERO_PERCENT_DISCOUNT);
  });
})